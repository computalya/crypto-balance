## crypto_balance.rb
 
example script to calculate current total balance of the crypto currencies listed in 'deposit' hash

```bash
./crypto_balance.rb
```

### example output

```bash

$ ruby crypto_balance.rb
                                     Balance     TRY Balance     USD Balance
                                     -------     -----------     -----------
      USDTRY           3.749           0 USD           0 TRY           0 USD
   mIOTA/USD          3.5254        10 mIOTA      132.30 TRY       35.30 USD
      BTCUSD         14271.0   0.0100609 BTC      538.28 TRY      143.58 USD

                                     Total :      670.58 TRY      178.88 USD
```

### Installation

download crypto_balance.rb

create config.json in the same directory

```bash
# config.json
{
  "access_key": "your-access-key-from-fixer-io"
}
```


