#!/usr/bin/env ruby
# crypto_balance.rb

# calculate current total balance of the crypto currencies
# listed in 'deposit' hash

# requirements
require 'JSON'
require 'uri'
require 'net/http'

# methods
def get_last_price(currency)
  url = URI("https://api.bitfinex.com/v1/pubticker/#{currency}")

  http = Net::HTTP.new(url.host, url.port)
  http.use_ssl = true

  request = Net::HTTP::Get.new(url)

  response = http.request(request)
  JSON.parse(response.read_body)['last_price']
end

def print_row(array, size, header = nil)
  array.each do |i|
    # printf i.to_s.center(size)
    printf i.to_s.rjust(size)
  end
  puts

  if header
    array.each do |i|
      (printf ('-' * i.size).rjust(size))
    end
    puts
  end
end

def get_rates
  uri = URI(API_URL)
  response = Net::HTTP.get_response(uri)

  JSON.parse(response.read_body)['rates']
end

# variables
config_file = 'config.json'

# main
# read access_key
file = File.read(config_file)
ACCESS_KEY = JSON.parse(file)['access_key']

API_URL = "http://data.fixer.io/api/latest?access_key=#{ACCESS_KEY}&format=1"

rates = get_rates

deposit = { iota: 20000000, btc: 0.00306794 }

USDTRY = (rates['TRY'] / rates['USD']).round(3)

# get prices
IOTUSD = get_last_price('iotusd')
BTCUSD = get_last_price('btcusd')

# calculations
# 1 iota
i_usd = sprintf( '%0.08f', (IOTUSD.to_f / 1000000))
i_try = sprintf( '%0.08f', (i_usd.to_f * USDTRY.to_f))

# balances
balance_iota_usd = sprintf( '%0.2f', (deposit[:iota] * i_usd.to_f))
balance_iota_try = sprintf( '%0.2f', (deposit[:iota] * i_try.to_f))

balance_btc_usd = sprintf( '%0.2f', (deposit[:btc] * BTCUSD.to_f))
balance_btc_try = balance_btc_usd.to_f * USDTRY.to_f

# output
header = [ '', '', 'Balance', 'TRY Balance', 'USD Balance' ]
print_row(header, 16, true)

array = [ 'USDTRY',  "#{USDTRY}", '0 USD', '0 TRY', '0 USD' ]
print_row(array, 16)

array = [ "mIOTA/USD", "#{IOTUSD}", "#{deposit[:iota] / 1000000} mIOTA", "#{balance_iota_try} TRY", "#{balance_iota_usd} USD" ]
print_row(array, 16)

array = [ "BTCUSD",  "#{BTCUSD}", "#{deposit[:btc]} BTC", "#{sprintf( '%0.2f', balance_btc_try)} TRY", "#{balance_btc_usd} USD" ]
print_row(array, 16)
puts

array = [ '', '', 'Total :', 
          "#{sprintf( '%0.2f', balance_btc_try.to_f + balance_iota_try.to_f)} TRY",
          "#{sprintf( '%0.2f', balance_btc_usd.to_f + balance_iota_usd.to_f)} USD" ]
print_row(array, 16)
