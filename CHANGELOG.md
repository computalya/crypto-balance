## Changelog

| date        | notes                                       |
|-------------|---------------------------------------------|
| 2018-08-08  | round USDTRY output                         |
| 2018-08-07  | use [fixer.io](fixer.io) to get USDTRY      |